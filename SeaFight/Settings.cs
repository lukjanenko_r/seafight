﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace SeaFight
{
    public partial class Settings : Form
    {
        MainForm mainForm;

        private string[] names, portLocals, portRemotes, IPadressRemotes;

        public Settings(MainForm f)
        {
            InitializeComponent();
            mainForm = f;
        }

        private void Settings_Load(object sender, EventArgs e)
        {
            DesktopLocation = new Point(
                mainForm.Location.X + mainForm.Size.Width - mainForm.Size.Width / 2 - Size.Width / 2,
                mainForm.Location.Y + mainForm.Size.Height - mainForm.Size.Height / 2 - Size.Height / 2);
            loadSetting();
        }

        private void loadSetting()
        {
            comboBoxServerNickName.Items.Clear();
            comboBoxSendPort.Items.Clear();
            comboBoxServerPort.Items.Clear();
            comboBoxIP.Items.Clear();
            if (Properties.Settings.Default.nameUser.Length != 0)
            {

                string str = Properties.Settings.Default.nameUser;
                names = str.Split(' ');

                for (int i = 0; i < names.Length; i++)
                {
                    comboBoxServerNickName.Items.Add(names[i]);
                }
            }

            if (Properties.Settings.Default.portLocal.Length != 0)
            {
                string str = Properties.Settings.Default.portLocal;
                portLocals = str.Split(' ');

                for (int i = 0; i < portLocals.Length; i++)
                {
                    comboBoxSendPort.Items.Add(portLocals[i]);
                }
            }

            if (Properties.Settings.Default.portRemote.Length != 0)
            {
                string str = Properties.Settings.Default.portRemote;
                portRemotes = str.Split(' ');

                for (int i = 0; i < portRemotes.Length; i++)
                {
                    comboBoxServerPort.Items.Add(portRemotes[i]);
                }
            }

            if (Properties.Settings.Default.IPadressRemote.Length != 0)
            {
                string str = Properties.Settings.Default.IPadressRemote;
                IPadressRemotes = str.Split(' ');

                for (int i = 0; i < IPadressRemotes.Length; i++)
                {
                    comboBoxIP.Items.Add(IPadressRemotes[i]);
                }
            }
        }

        private void ClearSelected_Click(object sender, EventArgs e)
        {

            Properties.Settings.Default.nameUser = "";
            for (int i = 0; i < names.Length; i++)
            {
                if (names[i] != comboBoxServerNickName.Text)
                {
                    Properties.Settings.Default.nameUser += names[i] + " ";
                }
            }
            Properties.Settings.Default.nameUser.Trim();


            Properties.Settings.Default.portLocal = "";
            for (int i = 0; i < portLocals.Length; i++)
            {
                if (portLocals[i] != comboBoxSendPort.Text)
                {
                    Properties.Settings.Default.portLocal += portLocals[i] + " ";
                }
            }
            Properties.Settings.Default.portLocal.Trim();



            Properties.Settings.Default.portRemote = "";
            for (int i = 0; i < portRemotes.Length; i++)
            {
                if (portRemotes[i] != comboBoxServerPort.Text)
                {
                    Properties.Settings.Default.portRemote += portRemotes[i] + " ";
                }
            }
            Properties.Settings.Default.portRemote.Trim();



            Properties.Settings.Default.IPadressRemote = "";
            for (int i = 0; i < IPadressRemotes.Length; i++)
            {
                if (IPadressRemotes[i] != comboBoxIP.Text)
                {
                    Properties.Settings.Default.IPadressRemote += IPadressRemotes[i] + " ";
                }
            }
            Properties.Settings.Default.IPadressRemote.Trim();


            comboBoxIP.Text = "";
            comboBoxServerPort.Text = "";
            comboBoxSendPort.Text = "";
            comboBoxServerNickName.Text = "";

            Properties.Settings.Default.Save();
            loadSetting();
        }

        private void ClearAll_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.portRemote = "";
            Properties.Settings.Default.IPadressRemote = "";
            Properties.Settings.Default.portLocal = "";
            Properties.Settings.Default.nameUser = "";
            Properties.Settings.Default.Save();
            loadSetting();
        }
    }
}
