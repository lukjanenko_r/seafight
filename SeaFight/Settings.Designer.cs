﻿namespace SeaFight
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxIP = new System.Windows.Forms.ComboBox();
            this.comboBoxServerPort = new System.Windows.Forms.ComboBox();
            this.comboBoxSendPort = new System.Windows.Forms.ComboBox();
            this.comboBoxServerNickName = new System.Windows.Forms.ComboBox();
            this.ClearAll = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ClearSelected = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // comboBoxIP
            // 
            this.comboBoxIP.FormattingEnabled = true;
            this.comboBoxIP.Location = new System.Drawing.Point(91, 12);
            this.comboBoxIP.Name = "comboBoxIP";
            this.comboBoxIP.Size = new System.Drawing.Size(128, 21);
            this.comboBoxIP.TabIndex = 8;
            // 
            // comboBoxServerPort
            // 
            this.comboBoxServerPort.FormattingEnabled = true;
            this.comboBoxServerPort.Location = new System.Drawing.Point(91, 38);
            this.comboBoxServerPort.Name = "comboBoxServerPort";
            this.comboBoxServerPort.Size = new System.Drawing.Size(128, 21);
            this.comboBoxServerPort.TabIndex = 9;
            // 
            // comboBoxSendPort
            // 
            this.comboBoxSendPort.FormattingEnabled = true;
            this.comboBoxSendPort.Location = new System.Drawing.Point(91, 64);
            this.comboBoxSendPort.Name = "comboBoxSendPort";
            this.comboBoxSendPort.Size = new System.Drawing.Size(128, 21);
            this.comboBoxSendPort.TabIndex = 11;
            // 
            // comboBoxServerNickName
            // 
            this.comboBoxServerNickName.FormattingEnabled = true;
            this.comboBoxServerNickName.Location = new System.Drawing.Point(91, 92);
            this.comboBoxServerNickName.Name = "comboBoxServerNickName";
            this.comboBoxServerNickName.Size = new System.Drawing.Size(128, 21);
            this.comboBoxServerNickName.TabIndex = 12;
            // 
            // ClearAll
            // 
            this.ClearAll.Location = new System.Drawing.Point(123, 121);
            this.ClearAll.Name = "ClearAll";
            this.ClearAll.Size = new System.Drawing.Size(96, 23);
            this.ClearAll.TabIndex = 15;
            this.ClearAll.Text = "Clear all";
            this.ClearAll.UseVisualStyleBackColor = true;
            this.ClearAll.Click += new System.EventHandler(this.ClearAll_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Send Port";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "IP";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "NickName";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Receive Port";
            // 
            // ClearSelected
            // 
            this.ClearSelected.Location = new System.Drawing.Point(14, 121);
            this.ClearSelected.Name = "ClearSelected";
            this.ClearSelected.Size = new System.Drawing.Size(96, 23);
            this.ClearSelected.TabIndex = 14;
            this.ClearSelected.Text = "Clear selected";
            this.ClearSelected.UseVisualStyleBackColor = true;
            this.ClearSelected.Click += new System.EventHandler(this.ClearSelected_Click);
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(234, 158);
            this.Controls.Add(this.comboBoxIP);
            this.Controls.Add(this.comboBoxServerPort);
            this.Controls.Add(this.comboBoxSendPort);
            this.Controls.Add(this.comboBoxServerNickName);
            this.Controls.Add(this.ClearAll);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ClearSelected);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Settings";
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.Settings_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxIP;
        private System.Windows.Forms.ComboBox comboBoxServerPort;
        private System.Windows.Forms.ComboBox comboBoxSendPort;
        private System.Windows.Forms.ComboBox comboBoxServerNickName;
        private System.Windows.Forms.Button ClearAll;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ClearSelected;
    }
}