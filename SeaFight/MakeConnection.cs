﻿using System;
using System.Drawing;
using System.Net;
using System.Windows.Forms;

namespace SeaFight
{
    public partial class MakeConnection : Form
    {
        private MainForm mainForm;

        public MakeConnection(MainForm f)
        {
            InitializeComponent();
            mainForm = f;
        }

        private void MakeConnection_Load(object sender, EventArgs e)
        {
            ControlBox = false;

            DesktopLocation = new Point(
                mainForm.Location.X + mainForm.Size.Width - mainForm.Size.Width / 2 - Size.Width / 2,
                mainForm.Location.Y + mainForm.Size.Height - mainForm.Size.Height / 2 - Size.Height / 2);
            
            if (Properties.Settings.Default.nameUser.Length != 0)
            {
                string str = Properties.Settings.Default.nameUser;
                string[] names = str.Split(' ');
                
                for (int i = 0; i < names.Length; i++)
                {
                    comboBoxServerNickName.Items.Add(names[i]);
                }
                comboBoxServerNickName.Text = names[names.Length - 1];
            }

            if (Properties.Settings.Default.portLocal.Length != 0)
            {
                string str = Properties.Settings.Default.portLocal;
                string[] portLocals = str.Split(' ');

                for (int i = 0; i < portLocals.Length; i++)
                {
                    comboBoxSendPort.Items.Add(portLocals[i]);
                }
                comboBoxSendPort.Text = portLocals[portLocals.Length - 1];
            }

            if (Properties.Settings.Default.portRemote.Length != 0)
            {
                string str = Properties.Settings.Default.portRemote;
                string[] portRemotes = str.Split(' ');

                for (int i = 0; i < portRemotes.Length; i++)
                {
                    comboBoxServerPort.Items.Add(portRemotes[i]);
                }
                comboBoxServerPort.Text = portRemotes[portRemotes.Length - 1];
            }

            if (Properties.Settings.Default.IPadressRemote.Length != 0)
            {
                string str = Properties.Settings.Default.IPadressRemote;
                string[] IPadressRemotes = str.Split(' ');

                for (int i = 0; i < IPadressRemotes.Length; i++)
                {
                    comboBoxIP.Items.Add(IPadressRemotes[i]);
                }
                comboBoxIP.Text = IPadressRemotes[IPadressRemotes.Length - 1];
            }
        }
        
        private void ok()
        {
            try
            {
                // Получаем данные, необходимые для соединения
                mainForm.localPort = Convert.ToInt16(comboBoxSendPort.Text);
                mainForm.remotePort = Convert.ToInt16(comboBoxServerPort.Text);
                mainForm.remoteIPAddress = IPAddress.Parse(comboBoxIP.Text);

                if ((comboBoxServerNickName.Text.Length != 0) && (!comboBoxServerNickName.Text.Contains(" ")))
                {
                    mainForm.nameThis = comboBoxServerNickName.Text;
                    mainForm.labelThis.Text = mainForm.nameThis;
                }
                else
                {
                    MessageBox.Show("NickName should not be empty and should not have space character ' '.");
                    return;
                }

                if (mainForm.localPort == mainForm.remotePort)
                {
                    MessageBox.Show("Local and remote ports should be different.");
                    return;
                }

                addSettings(comboBoxIP.Text, comboBoxServerPort.Text, comboBoxSendPort.Text, mainForm.nameThis);
            }
            catch
            {
                MessageBox.Show("Please fill correctly all the fields.");
                return;
            }

            mainForm.doConnection();
            mainForm.disconnectToolStripMenuItem1.Enabled = true;
            mainForm.connectToolStripMenuItem.Enabled = false;
            Close();
        }

        private void addSettings(string IP, string portR, string portL, string n)
        {
            string str = Properties.Settings.Default.nameUser;
            string[] names = str.Split(' ');

            bool isBreak = false;
            for (int i = 0; i < names.Length; i++)
            {
                if (names[i] == n)
                {
                    isBreak = true;
                    break;
                }
            }

            if (!isBreak)
            {
                Properties.Settings.Default.nameUser += " " + n;
                Properties.Settings.Default.Save();
            }


            str = Properties.Settings.Default.portLocal;
            string[] portLocals = str.Split(' ');

            isBreak = false;
            for (int i = 0; i < portLocals.Length; i++)
            {
                if (portLocals[i] == portL)
                {
                    isBreak = true;
                    break;
                }
            }

            if (!isBreak)
            {
                Properties.Settings.Default.portLocal += " " + portL;
                Properties.Settings.Default.Save();
            }


            str = Properties.Settings.Default.portLocal;
            string[] portRemotes = str.Split(' ');

            isBreak = false;
            for (int i = 0; i < portRemotes.Length; i++)
            {
                if (portRemotes[i] == portR)
                {
                    isBreak = true;
                    break;
                }
            }

            if (!isBreak)
            {
                Properties.Settings.Default.portRemote += " " + portR;
                Properties.Settings.Default.Save();
            }


            str = Properties.Settings.Default.IPadressRemote;
            string[] IPadressRemotes = str.Split(' ');

            isBreak = false;
            for (int i = 0; i < IPadressRemotes.Length; i++)
            {
                if (IPadressRemotes[i] == IP)
                {
                    isBreak = true;
                    break;
                }
            }

            if (!isBreak)
            {
                Properties.Settings.Default.IPadressRemote += " " + IP;
                Properties.Settings.Default.Save();
            }
        }
        
        private void comboBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ok();
            }
        }

        private void buttonConnect_Click(object sender, EventArgs e)
        {
            ok();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            mainForm.disconnectToolStripMenuItem1.Enabled = false;
            mainForm.connectToolStripMenuItem.Enabled = true;
            Close();
        }
    }
}
