﻿using System.Drawing;

namespace SeaFight
{
    class Cells
    {
        public bool isPushed = false;
        public bool isShip = false;
        public bool isRecurtion = true;

        public bool isVert = false; //для расстановки кораблей
        public int shipDecks;       //убрать корабль с поля расстановки

        public int x, y, cellSize;

        public Cells(int x1, int y1, int cellSize1)
        {
            x = x1;
            y = y1;
            cellSize = cellSize1;
        }

        public Rectangle getRect()
        {
            Rectangle rect = new Rectangle(new Point(x, y), new Size(cellSize, cellSize));
            return rect;
        }
    }
}
