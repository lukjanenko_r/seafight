﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace SeaFight
{
    public partial class DottingShips : Form
    {
        #region Vars
        private MainForm mainForm;
        internal Cells[,] cellsMy;

        Cells[] cells4 = new Cells[4];
        Cells[] cells3 = new Cells[3];
        Cells[] cells2 = new Cells[2];
        Cells[] cells1 = new Cells[1];

        Cells cell = new Cells(0, 0, 0);

        private Point mouseP = new Point();

        int cells4Quality, cells3Quality, cells2Quality, cells1Quality;

        bool isClose, isClickDown, isVert;
        int fieldSize, cellsQuality;
        int cellSize = 20;
        #endregion

        #region FormEvents
        public DottingShips(MainForm f)
        {
            InitializeComponent();
            mainForm = f;
        }

        private void DottingShips_Load(object sender, EventArgs e)
        {
            ControlBox = false;

            DesktopLocation = new Point(
                mainForm.Location.X + mainForm.Size.Width - mainForm.Size.Width / 2 - Size.Width / 2,
                mainForm.Location.Y + mainForm.Size.Height - mainForm.Size.Height / 2 - Size.Height / 2);

            fieldSize = mainForm.fs;
            cellsMy = new Cells[fieldSize, fieldSize];

            for (int i = 0; i < fieldSize; i++)
            {
                for (int j = 0; j < fieldSize; j++)
                {
                    cellsMy[i,j] = new Cells(20 + i * cellSize, 35 + j * cellSize, cellSize);
                }
            }

            cells4[0] = new Cells(246, 13, cellSize);
            cells4[1] = new Cells(246 + cellSize, 13, cellSize);
            cells4[2] = new Cells(246 + cellSize * 2, 13, cellSize);
            cells4[3] = new Cells(246 + cellSize * 3, 13, cellSize);

            cells3[0] = new Cells(266, 101, cellSize);
            cells3[1] = new Cells(266 + cellSize, 101, cellSize);
            cells3[2] = new Cells(266 + cellSize * 2, 101, cellSize);

            cells2[0] = new Cells(286, 168, cellSize);
            cells2[1] = new Cells(286 + cellSize, 168, cellSize);

            cells1[0] = new Cells(306, 216, cellSize);

            cells4Quality = 1;
            cells3Quality = 2;
            cells2Quality = 3;
            cells1Quality = 4;
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            if (cells4[0].isVert) cells4[0].isVert = false;
            else cells4[0].isVert = true;
            changeXY(ref cells4);
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (cells3[0].isVert) cells3[0].isVert = false;
            else cells3[0].isVert = true;
            changeXY(ref cells3);
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (cells2[0].isVert) cells2[0].isVert = false;
            else cells2[0].isVert = true;
            changeXY(ref cells2);
        }

        private void DottingShips_FormClosing(object sender, FormClosingEventArgs e)
        {
            if ((e.CloseReason == CloseReason.UserClosing) && !isClose)
            {
                e.Cancel = true;
            }
        }

        private void DottingShips_Paint(object sender, PaintEventArgs e)
        {
            Graphics g1 = e.Graphics;

            Bitmap bm = new Bitmap(416, 320, g1);
            Graphics g = Graphics.FromImage(bm);

            Font font = new Font("Arial", 13);

            g.FillRectangle(Brushes.White,
                cellsMy[0, 0].x - cellSize / 1.2f,
                cellsMy[0, 0].y - cellSize / 1.2f,
                (fieldSize + 1.44f) * cellSize,
                (fieldSize + 1.44f) * cellSize);

            for (int i = 0; i < fieldSize; i++)
            {
                g.DrawLine(new Pen(Color.FromArgb(181, 226, 239)),
                    cellsMy[i, 0].x,
                    cellsMy[i, 0].y - cellSize / 2.0f,
                    cellsMy[i, 0].x,
                    cellsMy[i, 0].y + (cellSize + 1) * fieldSize * 1.0f);
                g.DrawLine(new Pen(Color.FromArgb(181, 226, 239)),
                    cellsMy[0, i].x - cellSize / 2.0f,
                    cellsMy[0, i].y,
                    cellsMy[0, i].x + (cellSize + 1) * fieldSize * 1.0f,
                    cellsMy[0, i].y);

                if (i == 8)
                {
                    g.DrawString(((char)(65 + i)).ToString(), font,
                        new SolidBrush(Color.FromArgb(86, 49, 196)),
                        cellsMy[0, i].x - cellSize * 0.7f,
                        cellsMy[0, i].y);
                }
                else
                {
                    g.DrawString(((char)(65 + i)).ToString(), font,
                        new SolidBrush(Color.FromArgb(86, 49, 196)),
                        cellsMy[0, i].x - cellSize * 0.9f,
                        cellsMy[0, i].y);
                }

                if (i == fieldSize - 1)
                {
                    g.DrawLine(new Pen(Color.FromArgb(181, 226, 239)),
                        cellsMy[i, 0].x + cellSize,
                        cellsMy[i, 0].y - cellSize / 2.0f,
                        cellsMy[i, 0].x + cellSize,
                        cellsMy[i, 0].y + (cellSize + 1) * fieldSize * 1.0f);
                    g.DrawLine(new Pen(Color.FromArgb(181, 226, 239)),
                        cellsMy[0, i].x - cellSize / 2.0f,
                        cellsMy[0, i].y + cellSize,
                        cellsMy[0, i].x + (cellSize + 1) * fieldSize * 1.0f,
                        cellsMy[0, i].y + cellSize);


                    g.DrawString((i + 1).ToString(), font,
                        new SolidBrush(Color.FromArgb(86, 49, 196)),
                        cellsMy[i, 0].x - cellSize / 5.0f,
                        cellsMy[i, 0].y - cellSize);
                }
                else
                {
                    g.DrawString((i + 1).ToString(), font,
                        new SolidBrush(Color.FromArgb(86, 49, 196)),
                        cellsMy[i, 0].x + cellSize / 8.0f,
                        cellsMy[i, 0].y - cellSize);
                }
            }

            for (int i = 0; i < fieldSize; i += fieldSize - 1)
            {
                //Внешние границы "ручкой"
                if ((i == 0))
                {
                    g.DrawLine(new Pen(Color.FromArgb(86, 49, 196), 2.0f),
                        cellsMy[i, 0].x,
                        cellsMy[i, 0].y,
                        cellsMy[i, 0].x,
                        cellsMy[i, 0].y + cellSize * fieldSize);
                    g.DrawLine(new Pen(Color.FromArgb(86, 49, 196), 2.0f),
                        cellsMy[0, i].x,
                        cellsMy[0, i].y,
                        cellsMy[0, i].x + cellSize * fieldSize,
                        cellsMy[0, i].y);
                }
                if (i == fieldSize - 1)
                {
                    g.DrawLine(new Pen(Color.FromArgb(86, 49, 196), 2.0f),
                        cellsMy[i, 0].x + cellSize,
                        cellsMy[i, 0].y,
                        cellsMy[i, 0].x + cellSize,
                        cellsMy[i, 0].y + cellSize * fieldSize);
                    g.DrawLine(new Pen(Color.FromArgb(86, 49, 196), 2.0f),
                        cellsMy[0, i].x,
                        cellsMy[0, i].y + cellSize,
                        cellsMy[0, i].x + cellSize * fieldSize,
                        cellsMy[0, i].y + cellSize);
                }
            }

            //корабли для расстановки
            for (int i = 0; i < cells4.Length; i++)
            {
                g.FillRectangle(Brushes.White, cells4[i].getRect());
                g.DrawRectangle(new Pen(Color.FromArgb(86, 49, 196), 2.0f), cells4[i].getRect());
            }
            for (int i = 0; i < cells3.Length; i++)
            {
                g.FillRectangle(Brushes.White, cells3[i].getRect());
                g.DrawRectangle(new Pen(Color.FromArgb(86, 49, 196), 2.0f), cells3[i].getRect());
            }
            for (int i = 0; i < cells2.Length; i++)
            {
                g.FillRectangle(Brushes.White, cells2[i].getRect());
                g.DrawRectangle(new Pen(Color.FromArgb(86, 49, 196), 2.0f), cells2[i].getRect());
            }
            g.FillRectangle(Brushes.White, cells1[0].getRect());
            g.DrawRectangle(new Pen(Color.FromArgb(86, 49, 196), 2.0f), cells1[0].getRect());

            g.DrawRectangle(Pens.Black, 243, 10, 150, 85);
            g.DrawRectangle(Pens.Black, 243, 98, 150, 65);
            g.DrawRectangle(Pens.Black, 243, 165, 150, 45);
            g.DrawRectangle(Pens.Black, 243, 213, 150, 25);

            g.DrawString(cells4Quality.ToString(), new Font("Arial", 16), Brushes.Red, 330, 30);
            g.DrawString(cells3Quality.ToString(), new Font("Arial", 16), Brushes.Red, 330, 110);
            g.DrawString(cells2Quality.ToString(), new Font("Arial", 16), Brushes.Red, 330, 170);
            g.DrawString(cells1Quality.ToString(), new Font("Arial", 16), Brushes.Red, 330, 215);

            //корабли
            for (int i = 0; i < fieldSize; i++)
            {
                for (int j = 0; j < fieldSize; j++)
                {
                    if (cellsMy[i, j].isShip)
                    {
                        if (isLeftLine(i, j))
                            g.DrawLine(new Pen(Color.FromArgb(86, 49, 196), 2),
                                cellsMy[i, j].x, cellsMy[i, j].y,
                                cellsMy[i, j].x, cellsMy[i, j].y + cellSize);
                        if (isRightLine(i, j))
                            g.DrawLine(new Pen(Color.FromArgb(86, 49, 196), 2),
                                cellsMy[i, j].x + cellSize, cellsMy[i, j].y,
                                cellsMy[i, j].x + cellSize, cellsMy[i, j].y + cellSize);
                        if (isUpLine(i, j))
                            g.DrawLine(new Pen(Color.FromArgb(86, 49, 196), 2),
                                cellsMy[i, j].x, cellsMy[i, j].y,
                                cellsMy[i, j].x + cellSize, cellsMy[i, j].y);
                        if (isDownLine(i, j))
                            g.DrawLine(new Pen(Color.FromArgb(86, 49, 196), 2),
                                cellsMy[i, j].x, cellsMy[i, j].y + cellSize,
                                cellsMy[i, j].x + cellSize, cellsMy[i, j].y + cellSize);
                    }
                }
            }

            if (isClickDown)
            {
                for (int i = 0; i < cellsQuality; i++)
                {
                    if (isVert)
                    {
                        g.TranslateTransform(0, i * cellSize);
                        g.FillRectangle(Brushes.White, cell.getRect());
                        g.DrawRectangle(new Pen(Color.FromArgb(86, 49, 196), 2.0f), cell.getRect());
                        g.TranslateTransform(0, -i * cellSize);
                    }
                    else
                    {
                        g.TranslateTransform(i * cellSize, 0);
                        g.FillRectangle(Brushes.White, cell.getRect());
                        g.DrawRectangle(new Pen(Color.FromArgb(86, 49, 196), 2.0f), cell.getRect());
                        g.TranslateTransform(-i * cellSize, 0);
                    }
                }
            }

            g1.DrawImage(bm, 0, 0);
        }
        #endregion
        
        #region isLinesForDrawingShip
        private bool isLeftLine(int i, int j)
        {
            if (i - 1 >= 0)
                if (cellsMy[i - 1, j].isShip) return false;
                else return true;
            else
                return false;
        }

        private bool isRightLine(int i, int j)
        {
            if (i + 1 < fieldSize)
                if (cellsMy[i + 1, j].isShip) return false;
                else return true;
            else
                return false;
        }

        private bool isUpLine(int i, int j)
        {
            if (j - 1 >= 0)
                if (cellsMy[i, j  - 1].isShip) return false;
                else return true;
            else
                return false;
        }

        private bool isDownLine(int i, int j)
        {
            if (j + 1 < fieldSize)
                if (cellsMy[i, j + 1].isShip) return false;
                else return true;
            else
                return false;
        }
        #endregion
        
        #region MouseButtonsClicks
        private void DottingShips_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                bool isHit = false;
                for (int i = 0; i < 1; i++)
                {
                    isClickOnShip(cells1, 1, e.X, e.Y, ref isHit);
                    if (isHit) break;
                    isClickOnShip(cells2, 2, e.X, e.Y, ref isHit);
                    if (isHit) break;
                    isClickOnShip(cells3, 3, e.X, e.Y, ref isHit);
                    if (isHit) break;
                    isClickOnShip(cells4, 4, e.X, e.Y, ref isHit);
                }
            }

            if (e.Button == MouseButtons.Right)
            {
                for (int i = 0; i < fieldSize; i++)
                {
                    bool isBreak = false;
                    for (int j = 0; j < fieldSize; j++)
                    {
                        if (cellsMy[i, j].getRect().Contains(new Point(e.X, e.Y)))
                        {
                            isShipReturn(i, j);
                            isBreak = true;
                            break;
                        }
                    }
                    if (isBreak) break;
                }
            }
        }
        
        private void DottingShips_MouseUp(object sender, MouseEventArgs e)
        {
            if (isClickDown)
            {
                for (int i = 0; i < fieldSize; i++)
                {
                    bool isBreak = false;
                    for (int j = 0; j < fieldSize; j++)
                    {
                        if (cellsMy[i, j].getRect().Contains(new Point(e.X, e.Y)))
                        {
                            if (isAbleToAddShip(i, j))
                            {
                                addShip(i, j);
                                minusShip();
                                isBreak = true;
                                break;
                            }
                        }
                    }
                    if (isBreak) break;
                }
            }

            isClickDown = false;
            Invalidate();
        }

        private void DottingShips_MouseMove(object sender, MouseEventArgs e)
        {
            if (isClickDown)
            {
                returnMousePosition(e.X, e.Y);
                Invalidate();
            }
        }

        private void buttonRandomDot_Click(object sender, EventArgs e)
        {
            reset();

            addRandomShip(4, 1);
            addRandomShip(3, 2);
            addRandomShip(2, 3);
            addRandomShip(1, 4);

            isVert = false;
            mainForm.isRandom = true;

            cells4Quality = 0;
            cells3Quality = 0;
            cells2Quality = 0;
            cells1Quality = 0;

            Invalidate();
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            reset();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if ((cells4Quality == 0) && (cells3Quality == 0) && (cells2Quality == 0) && (cells1Quality == 0))
            {
                mainForm.isSendReady = true;

                isClose = true;
                Close();
            }
            else
            {
                MessageBox.Show("You should dotting all the ships to the field.");
            }
        }
        #endregion

        #region ForMouse
        private void changeXY(ref Cells[] cells)
        {
            if (!cells[0].isVert)
            {
                for (int i = 0; i < cells.Length; i++)
                {
                    cells[i].x = cells[0].x + cellSize * i;
                    cells[i].y = cells[0].y;
                }
            }
            else
            {
                for (int i = 0; i < cells.Length; i++)
                {
                    cells[i].x = cells[0].x;
                    cells[i].y = cells[0].y + cellSize * i;
                }
            }
            Invalidate();
        }

        private void returnMousePosition(int xx, int yy)
        {
            mouseP.X = xx;
            mouseP.Y = yy;

            cell.x = xx;
            cell.y = yy;
            cell.cellSize = cellSize;
        }

        private void isClickOnShip(Cells[] cells, int cellQual, int xx, int yy, ref bool isHit)
        {
            bool isShipLeft = false;
            switch (cellQual)
            {
                case 1:
                    if (cells1Quality > 0) isShipLeft = true;
                    break;
                case 2:
                    if (cells2Quality > 0) isShipLeft = true;
                    break;
                case 3:
                    if (cells3Quality > 0) isShipLeft = true;
                    break;
                case 4:
                    if (cells4Quality > 0) isShipLeft = true;
                    break;
            }
            if (isShipLeft)
            {
                for (int i = 0; i < cells.Length; i++)
                {
                    if (cells[i].getRect().Contains(new Point(xx, yy)))
                    {
                        isClickDown = true;
                        cellsQuality = cellQual;
                        isVert = cells[0].isVert;
                        returnMousePosition(xx, yy);
                        isHit = true;
                        Invalidate();
                        break;
                    }
                }
            }
        }
        #endregion

        #region Ships
        private void addRandomShip(int decks, int howMuchShips)
        {
            for (int i = 0; i < howMuchShips; i++)
            {
                while (true)
                {
                    cellsQuality = decks;
                    Random r = new Random(DateTime.Now.Millisecond);
                    int a = r.Next(0, fieldSize);
                    int b = r.Next(0, fieldSize);
                    int c = r.Next(0, 2);
                    if (c == 0) isVert = true;
                    else isVert = false;

                    if (isAbleToAddShip(a, b))
                    {
                        addShip(a, b);
                        break;
                    }
                }
            }
        }
        
        private void reset()
        {
            for (int i = 0; i < fieldSize; i++)
            {
                for (int j = 0; j < fieldSize; j++)
                {
                    cellsMy[i, j].isShip = false;
                }
            }
            cells4Quality = 1;
            cells3Quality = 2;
            cells2Quality = 3;
            cells1Quality = 4;
            Invalidate();
        }

        private void minusShip()
        {
            switch (cellsQuality)
            {
                case 1:
                    cells1Quality--;
                    break;
                case 2:
                    cells2Quality--;
                    break;
                case 3:
                    cells3Quality--;
                    break;
                case 4:
                    cells4Quality--;
                    break;
            }
        }

        private bool isAbleToAddShip(int i, int j)
        {
            bool isAble = false;

            if (!cellsMy[i, j].isShip)
            {
                if (isVert)
                {
                    if (j + cellsQuality <= fieldSize)
                        isAble = true;
                }
                else
                    if (i + cellsQuality <= fieldSize)
                    isAble = true;

                //проверка на смежные ячейки
                if (isAble)
                {
                    if (isVert)
                    {
                        for (int a = j; a < j + cellsQuality; a++)
                        {
                            isAble = relatedCellsTestMain(i, a);
                            if (!isAble) break;
                        }
                    }
                    else
                    {
                        for (int a = i; a < i + cellsQuality; a++)
                        {
                            isAble = relatedCellsTestMain(a, j);
                            if (!isAble) break;
                        }
                    }
                }
            }

            return isAble;
        }

        private bool relatedCellsTestMain(int i, int j)
        {
            bool isAble = true;

            for (int a = 0; a < 1; a++)
            {
                if (i - 1 >= 0)
                    isAble = relatedCellsTest(i - 1, j);
                if (!isAble) break;
                if ((i - 1 >= 0) && (j - 1 >= 0))
                    isAble = relatedCellsTest(i - 1, j - 1);
                if (!isAble) break;
                if ((i - 1 >= 0) && (j + 1 < fieldSize))
                    isAble = relatedCellsTest(i - 1, j + 1);
                if (!isAble) break;

                if (i + 1 < fieldSize)
                    isAble = relatedCellsTest(i + 1, j);
                if (!isAble) break;
                if ((i + 1 < fieldSize) && (j - 1 >= 0))
                    isAble = relatedCellsTest(i + 1, j - 1);
                if (!isAble) break;
                if ((i + 1 < fieldSize) && (j + 1 < fieldSize))
                    isAble = relatedCellsTest(i + 1, j + 1);
                if (!isAble) break;

                if (j + 1 < fieldSize)
                    isAble = relatedCellsTest(i, j + 1);
                if (!isAble) break;
                if (j - 1 >= 0)
                    isAble = relatedCellsTest(i, j - 1);
                if (!isAble) break;
            }

            return isAble;
        }

        private bool relatedCellsTest(int i, int j)
        {
            if (cellsMy[i, j].isShip) return false;
            else return true;
        }

        private void addShip(int a, int b)
        {
            for (int i = 0; i < cellsQuality; i++)
            {
                if (isVert)
                {
                    cellsMy[a, b + i].isShip = true;
                    cellsMy[a, b + i].shipDecks = cellsQuality;
                }
                else
                {
                    cellsMy[a + i, b].isShip = true;
                    cellsMy[a + i, b].shipDecks = cellsQuality;
                }
                mainForm.isRandom = false;
            }
        }

        private void isShipReturn(int i, int j)
        {
            if (cellsMy[i, j].isShip)
            {
                switch (cellsMy[i, j].shipDecks)
                {
                    case 1:
                        cells1Quality++;
                        break;
                    case 2:
                        cells2Quality++;
                        break;
                    case 3:
                        cells3Quality++;
                        break;
                    case 4:
                        cells4Quality++;
                        break;
                }
                returnCells(i, j);
            }
        }

        private void returnCells(int i, int j)
        {
            if (cellsMy[i, j].isShip)
            {
                cellsMy[i, j].isShip = false;
                if (i - 1 >= 0)
                    returnCells(i - 1, j);
                if ((i - 1 >= 0) && (j - 1 >= 0))
                    returnCells(i - 1, j - 1);
                if ((i - 1 >= 0) && (j + 1 < fieldSize))
                    returnCells(i - 1, j + 1);

                if (i + 1 < fieldSize)
                    returnCells(i + 1, j);
                if ((i + 1 < fieldSize) && (j - 1 >= 0))
                    returnCells(i + 1, j - 1);
                if ((i + 1 < fieldSize) && (j + 1 < fieldSize))
                    returnCells(i + 1, j + 1);

                if (j + 1 < fieldSize)
                    returnCells(i, j + 1);
                if (j - 1 >= 0)
                    returnCells(i, j - 1);
            }
        }
        #endregion
    }
}
