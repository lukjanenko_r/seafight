﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace SeaFight
{
    public partial class Rules : Form
    {
        MainForm mainForm;

        public Rules(MainForm f)
        {
            InitializeComponent();
            mainForm = f;
        }

        private void Rules_Load(object sender, EventArgs e)
        {
            DesktopLocation = new Point(
                mainForm.Location.X + mainForm.Size.Width - mainForm.Size.Width / 2 - Size.Width / 2,
                mainForm.Location.Y + mainForm.Size.Height - mainForm.Size.Height / 2 - Size.Height / 2);
        }

        private void rules_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                Close();
        }
    }
}
