﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace SeaFight
{
    public partial class MainForm : System.Windows.Forms.Form
    {
        #region Vars
        Rules rules;
        Settings settings;
        MakeConnection makeConnection;
        DottingShips dotShips;

        public string nameThis, nameContestant;

        public IPAddress remoteIPAddress;
        public int remotePort, localPort;
        public bool isConnection, isClose, isGame, isMyTurnPlay, isAnswer, isSecondStart;
        bool isLoose = false;
        public bool isRandom = false;
        public bool isSendReady = false;
        
        private bool isExtra, isMiss;

        private int correntI, correntJ, stepsMy, stepsContestant;
        private bool isPlusStepMy, isPlusStepContestant, isStepPossible;
        private bool isStep = false;

        delegate void AddMessageDelegate(string message);

        Thread tRec, tRec1;
        private bool tRecCheck = true;
        private bool tRec1Check = true;
        UdpClient receivingUdpClient, senderUdpClient;

        private const int fieldSize = 10;
        public int fs = fieldSize;

        Cells[,] cellsMy = new Cells[fieldSize, fieldSize];
        Cells[,] cellsContestant = new Cells[fieldSize, fieldSize];

        private int cellSize = 30;

        Stopwatch stopWatch = new Stopwatch();
        private int timeForStep = 30;

        private bool isConnectionReal = true;
        private bool isConnectionRealForTimer = false;

        System.Media.SoundPlayer soundExplosion = new System.Media.SoundPlayer(Properties.Resources.explosion);
        System.Media.SoundPlayer soundExplosionFinish = new System.Media.SoundPlayer(Properties.Resources.explosion_finish);
        System.Media.SoundPlayer soundLoose = new System.Media.SoundPlayer(Properties.Resources.loose);
        System.Media.SoundPlayer soundMiss = new System.Media.SoundPlayer(Properties.Resources.miss);
        System.Media.SoundPlayer soundWin = new System.Media.SoundPlayer(Properties.Resources.win);
        System.Media.SoundPlayer soundStandoff = new System.Media.SoundPlayer(Properties.Resources.standoff);

        private bool isRocket = false;
        private bool isEnter = false;
        private int x1, y1, x2, y2;
        private int step = 15;
        private Bitmap rocketBitmap = Properties.Resources.Rockets;
        private bool isUp, isDown, isLeft, isRight;
        #endregion

        #region GameLogic
        private void anyPushed()
        {
            for (int i = 0; i < fieldSize; i++)
            {
                for (int j = 0; j < fieldSize; j++)
                {
                    cellsMy[i, j].isPushed = false;

                    cellsContestant[i, j].isShip = false;
                    cellsContestant[i, j].isPushed = false;
                }
            }
        }
        
        private void resetField()
        {
            isClose = false;
            isGame = false;
            isMyTurnPlay = false;
            isAnswer = false;
            isSecondStart = false;
            isRandom = false;
            isSendReady = false;
            isLoose = false;
            
            startGame();
            anyPushed();
        }

        private void startGame()
        {
            stepsMy = 0;
            stepsContestant = 0;
            isPlusStepMy = true;
            isPlusStepContestant = true;
            isExtra = false;
            isMiss = false;
            
            dotShips = new DottingShips(this);
            dotShips.ShowDialog();
            
            for (int i = 0; i < fieldSize; i++)
            {
                for (int j = 0; j < fieldSize; j++)
                {
                    cellsMy[i, j].isShip = dotShips.cellsMy[i, j].isShip;
                    cellsMy[i, j].isRecurtion = true;
                    cellsMy[i, j].isPushed = false;

                    cellsContestant[i, j].isRecurtion = true;
                    cellsContestant[i, j].isPushed = false;
                }
            }

            Invalidate();
            addChat("Please wait while " + nameContestant + " will put the ships on the field.");
        }

        private void allAreReady()
        {
            if (!isSecondStart)
            {
                isSecondStart = true;
                if (isRandom) send(" random");
                else send(" no random");

                isGame = true;
                if (localPort < remotePort)
                {
                    isMyTurnPlay = true;
                    send(" my turn");
                }
                stopWatch.Start();
                isStep = false;
            }
        }

        private void isLost()
        {
            bool isL = true;
            for (int i = 0; i < fieldSize; i++)
            {
                bool isBreak = false;
                for (int j = 0; j < fieldSize; j++)
                {
                    if (cellsMy[i, j].isShip && !cellsMy[i, j].isPushed)
                    {
                        isL = false;
                        isBreak = true;
                        break;
                    }
                }
                if (isBreak)
                    break;
            }

            if (isL)
            {
                if (stepsMy != stepsContestant)
                {
                    isExtra = true;
                    send(" extra");
                    addChat(nameContestant + " has destroyed all your ships. You have the last chance for standoff becouse you`ve done less steps. Good luck, " + nameThis + "!");
                    send(" my turn");
                    isMyTurnPlay = true;
                }
                else
                {
                    if (!isExtra)
                    {
                        isGame = false;
                        send(" u win");
                        soundLoose.Play();
                        stopWatch.Reset();
                    }
                    else
                    {
                        isGame = false;
                        send(" standoff");
                        soundStandoff.Play();
                        addChat("You both are good players. Standoff this time.");
                        stopWatch.Reset();
                        MessageBox.Show("New game will start after you click 'ok'.");
                        resetField();
                    }
                }
            }
            else
            {
                if (isMiss)
                {
                    addChat("You`ve won! Congratulations! Good game!");
                    soundWin.Play();
                    isGame = false;
                    stopWatch.Reset();
                    send(" ok loser");
                    MessageBox.Show("New game will start after you click 'ok'.");
                    resetField();
                }
            }
        }

        private void isTimeEnd(int passedTime)
        {
            if ((passedTime > timeForStep) && isMyTurnPlay)
            {
                stopWatch.Reset();
                send(" time");
            }
        }

        private void pushOnCell(int i, int j)
        {
            correntI = i;
            correntJ = j;

            

            send(nameThis + " shoots to the cell " + (char)(j + 65) + (i + 1).ToString() + ".");
            addChat("You`ve shot cell " + (char)(j + 65) + (i + 1).ToString() + ".");

            send(" " + i.ToString() + j.ToString());

            isStepPossible = true;
        }

        private void handleGameData(string returnData)
        {
            stopWatch.Reset();
            stopWatch.Start();
            isStep = true;
            
            send(" connection good");
            
            int a = Convert.ToInt16(returnData.Substring(1, 1));
            int b = Convert.ToInt16(returnData.Substring(2, 1));

            correntI = a;
            correntJ = b;
            
            if (isPlusStepContestant)
            {
                isPlusStepContestant = false;
                stepsContestant++;
            }

            pushRockets(a, b, true);
            while (isRocket) { }

            cellsMy[a, b].isPushed = true;

            if (cellsMy[a, b].isShip)
            {
                string cok = cutOrKill(a, b);
                send(cok);
                isLost();
            }
            else
            {
                if (isExtra)
                {
                    isMiss = true;
                }
                isPlusStepContestant = true;
                isMyTurnPlay = true;

                send(" past");
                soundMiss.Play();
            }
        }

        private void cutShip(bool isMy)
        {
            if (isMy)
            {
                cellsMy[correntI, correntJ].isShip = true;
                cellsMy[correntI, correntJ].isPushed = true;
            }
            else
            {
                cellsContestant[correntI, correntJ].isShip = true;
                cellsContestant[correntI, correntJ].isPushed = true;
            }
            soundExplosion.Play();
        }

        private void killShip(bool isMy)
        {
            if (isMy)
            {
                cellsMy[correntI, correntJ].isShip = true;
                cellsMy[correntI, correntJ].isPushed = true;
                pushNearestCells(correntI, correntJ, true, ref cellsMy);
            }
            else
            {
                cellsContestant[correntI, correntJ].isShip = true;
                cellsContestant[correntI, correntJ].isPushed = true;
                pushNearestCells(correntI, correntJ, false, ref cellsContestant);
                
                send(" push");
            }
            soundExplosionFinish.Play();
        }

        private void pushNearestCells(int i, int j, bool isMy, ref Cells[,] c)
        {
            c[i, j].isRecurtion = false;
            if (i - 1 >= 0)
            {
                pushCell(i - 1, j, isMy);
                if (c[i - 1, j].isShip && c[i - 1, j].isRecurtion)
                    pushNearestCells(i - 1, j, isMy, ref c);
            }
            if ((i - 1 >= 0) && (j - 1 >= 0))
                pushCell(i - 1, j - 1, isMy);
            if ((i - 1 >= 0) && (j + 1 < fieldSize))
                pushCell(i - 1, j + 1, isMy);

            if (i + 1 < fieldSize)
            {
                pushCell(i + 1, j, isMy);
                if (c[i + 1, j].isShip && c[i + 1, j].isRecurtion)
                    pushNearestCells(i + 1, j, isMy, ref c);
            }
            if ((i + 1 < fieldSize) && (j - 1 >= 0))
                pushCell(i + 1, j - 1, isMy);
            if ((i + 1 < fieldSize) && (j + 1 < fieldSize))
                pushCell(i + 1, j + 1, isMy);

            if (j + 1 < fieldSize)
            {
                pushCell(i, j + 1, isMy);
                if (c[i, j + 1].isShip && c[i, j + 1].isRecurtion)
                    pushNearestCells(i, j + 1, isMy, ref c);
            }
            if (j - 1 >= 0)
            {
                pushCell(i, j - 1, isMy);
                if (c[i, j - 1].isShip && c[i, j - 1].isRecurtion)
                    pushNearestCells(i, j - 1, isMy, ref c);
            }
        }

        private void pushCell(int i, int j, bool isMy)
        {
            if (isMy)
            {
                cellsMy[i, j].isPushed = true;
            }
            else
            {
                cellsContestant[i, j].isPushed = true;
            }
        }

        private void isShipHit(bool isHit)
        {
            if (!isHit)
            {
                isMyTurnPlay = false;
                cellsContestant[correntI, correntJ].isShip = false;
                cellsContestant[correntI, correntJ].isPushed = true;
            }
            else
            {
                isMyTurnPlay = true;
                cellsContestant[correntI, correntJ].isShip = true;
                cellsContestant[correntI, correntJ].isPushed = true;
            }

            Invalidate();
        }

        private string cutOrKill(int i, int j)
        {
            if (cellsMy[i, j].isRecurtion)
            {
                cellsMy[i, j].isRecurtion = false;
                string cok = " kill";

                string isKill = " kill";

                for (int a = 0; a < 1; a++)
                {
                    if (i - 1 >= 0)
                        if (cellsMy[i - 1, j].isShip && !cellsMy[i - 1, j].isPushed)
                        {
                            cok = " cut";
                            break;
                        }
                    if (j - 1 >= 0)
                        if (cellsMy[i, j - 1].isShip && !cellsMy[i, j - 1].isPushed)
                        {
                            cok = " cut";
                            break;
                        }
                    if (i + 1 < fieldSize)
                        if (cellsMy[i + 1, j].isShip && !cellsMy[i + 1, j].isPushed)
                        {
                            cok = " cut";
                            break;
                        }
                    if (j + 1 < fieldSize)
                        if (cellsMy[i, j + 1].isShip && !cellsMy[i, j + 1].isPushed)
                        {
                            cok = " cut";
                            break;
                        }

                    if (cok == " kill")
                    {
                        if (i - 1 >= 0)
                            if (cellsMy[i - 1, j].isShip && cellsMy[i - 1, j].isPushed)
                            {
                                isKill = cutOrKill(i - 1, j);
                                if (isKill== " cut")
                                    break;
                            }
                        if (j - 1 >= 0)
                            if (cellsMy[i, j - 1].isShip && cellsMy[i, j - 1].isPushed)
                            {
                                isKill = cutOrKill(i, j - 1);
                                if (isKill == " cut")
                                    break;
                            }
                        if (i + 1 < fieldSize)
                            if (cellsMy[i + 1, j].isShip && cellsMy[i + 1, j].isPushed)
                            {
                                isKill = cutOrKill(i + 1, j);
                                if (isKill == " cut")
                                    break;
                            }
                        if (j + 1 < fieldSize)
                            if (cellsMy[i, j + 1].isShip && cellsMy[i, j + 1].isPushed)
                            {
                                isKill = cutOrKill(i, j + 1);
                                if (isKill == " cut")
                                    break;
                            }
                    }
                }

                cellsMy[i, j].isRecurtion = true;

                if (isKill == " cut")
                {
                    return isKill;
                }
                else
                    return cok;
            }
            else
                return " kill";
        }
        #endregion

        #region ConnectionAndMessages
        public void doConnection()
        {
            timer1.Enabled = true;
            try
            {
                // Создаем поток для прослушивания
                tRec = new Thread(new ThreadStart(receiver));
                tRec1 = new Thread(new ThreadStart(sendRecieveFirstMessage));
                tRec1.Start();

            }
            catch
            {
                //isConnection = false;
                addChat("Some problems with connection...");// + ex.ToString() + "\n  " + ex.Message);
            }
        }

        private void closeConnections()
        {
            isConnection = false;
            isMyTurnPlay = false;
            isAnswer = false;
            isGame = false;
            try
            {
                tRecCheck = false;

                receivingUdpClient.Close();
                senderUdpClient.Close();
            }
            catch { }
        }

        private void sendMessage()
        {
            if (textBoxForMessages.Text.Length != 0)
            {
                addChat(nameThis + ": " + textBoxForMessages.Text + "\n");
                send(nameThis + ": " + textBoxForMessages.Text);
                textBoxForMessages.Text = "";
            }
            textBoxForMessages.Focus();
        }

        private void sendRecieveFirstMessage()
        {
            senderUdpClient = new UdpClient();
            IPEndPoint endPoint = new IPEndPoint(remoteIPAddress, remotePort);
            addChat("Trying to connect...");

            receivingUdpClient = new UdpClient(localPort);
            IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, remotePort);

            while (tRec1Check)
            {
                try
                {
                    if (!isConnection)
                    {
                        byte[] bytes = Encoding.UTF8.GetBytes(nameThis);
                        senderUdpClient.Send(bytes, bytes.Length, endPoint);
                    }

                    // Ожидание дейтаграммы
                    byte[] receiveBytes = receivingUdpClient.Receive(ref RemoteIpEndPoint);

                    // Преобразуем и отображаем данные
                    string returnData = Encoding.UTF8.GetString(receiveBytes);

                    if (returnData.Length != 0)
                    {
                        send(nameThis);

                        nameContestant = returnData.ToString();
                        changeNameLabel(nameContestant);
                        isConnection = true;
                        
                        addChat("Connection with " + nameContestant + " is successful.");

                        startGame();

                        break;
                    }
                }
                catch
                {
                    addChat("Some problems with connection...");
                }
            }
            receivingUdpClient.Close();
            senderUdpClient.Close();
            tRec.Start();
            
            tRec1Check = false;
        }

        private void send(string datagram)
        {
            // Создаем UdpClient
            senderUdpClient = new UdpClient();

            // Создаем endPoint по информации об удаленном хосте
            IPEndPoint endPoint = new IPEndPoint(remoteIPAddress, remotePort);

            try
            {
                // Преобразуем данные в массив байтов
                byte[] bytes = Encoding.UTF8.GetBytes(datagram);

                // Отправляем данные
                senderUdpClient.Send(bytes, bytes.Length, endPoint);
            }
            catch
            {
                //isConnection = false;
                addChat("Some problems with connection...");
            }
            finally
            {
                // Закрыть соединение
                senderUdpClient.Close();
            }
        }

        public void receiver()
        {
            // Создаем UdpClient для чтения входящих данных
            receivingUdpClient = new UdpClient(localPort);

            IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, remotePort);

            try
            {
                while (tRecCheck)
                {
                    // Ожидание дейтаграммы
                    byte[] receiveBytes = receivingUdpClient.Receive(ref RemoteIpEndPoint);

                    // Преобразуем и отображаем данные
                    string returnData = Encoding.UTF8.GetString(receiveBytes);

                    if (returnData != nameContestant)
                    {
                        if (returnData.Substring(0, 1) != " ")
                            addChat(returnData);
                        else
                            handleReturnData(returnData);
                    }
                }
            }
            catch
            {
                //isConnection = false;
                addChat("Some problems with connection...");
            }
            finally
            {
                // Закрыть соединение
                receivingUdpClient.Close();
            }
        }

        private void addChat(string str)
        {
            str = str.Trim();
            str += "\n";

            if (richTextBoxChat.InvokeRequired)
            {
                AddMessageDelegate a = new AddMessageDelegate(addChat);
                richTextBoxChat.Invoke(a, new object[] { str });
            }
            else
            {
                richTextBoxChat.Text += str;
            }

            try
            {
                richTextBoxChat.SelectionStart = richTextBoxChat.Text.Length;
                richTextBoxChat.ScrollToCaret();
            }
            catch { }
        }
        #endregion

        //обработчкик сообщений
        private void handleReturnData(string returnData)
        {
            switch (returnData)
            {
                case " disconnect":
                    if (isGame)
                        addChat("Your contestant has surrendered. Congratulations, " + nameThis + "! Good game!");
                    addChat("Connection had been lost.");
                    closeConnections();
                    break;
                case " ready":
                    if (isSendReady)
                    {
                        send(" ready");
                        isSendReady = false;
                        addChat(nameContestant + " is ready for game.");
                        isAnswer = true;
                        allAreReady();
                    }
                    break;
                case " random":
                    addChat(nameContestant + " will use random ship placement.");
                    if (isMyTurnPlay)
                    {
                        addChat("Your turn to shoot now.");
                    }
                    else
                    {
                        addChat(nameContestant + " turn to shoot now.");
                    }
                    break;
                case " no random":
                    addChat(nameContestant + " will use manual ship placement.");
                    if (isMyTurnPlay)
                    {
                        addChat("Your turn to shoot now.");
                    }
                    else
                    {
                        addChat(nameContestant +" turn to shoot now.");
                    }
                    break;
                case " my turn":
                    isMyTurnPlay = false;
                    break;
                case " past":
                    isShipHit(false);
                    isPlusStepMy = true;
                    isPlusStepContestant = true;
                    if (!isExtra)
                    {
                        addChat("You`ve passed. " + nameContestant + " turn to shoot.");
                        soundMiss.Play();
                        send(nameThis + " has passed. Your turn to shoot.");
                    }
                    else
                    {
                        send(nameThis + " has passed.");
                        send(" u win");
                    }
                    isExtra = false;
                    break;
                case " cut":
                    cutShip(false);
                    addChat("You`ve hited the ship.");
                    send(nameThis + " has hited the ship.");
                    break;
                case " kill":
                    killShip(false);
                    addChat("You`ve destroyed the ship.");
                    send(nameThis + " has destroyed the ship.");
                    break;
                case " push":
                    pushNearestCells(correntI, correntJ, true, ref cellsMy);
                    break;
                case " u win":
                    isGame = false;
                    addChat("You`ve won! Congratulations! Good game!");
                    soundWin.Play();
                    stopWatch.Stop();
                    send(" ok loser");
                    MessageBox.Show("New game will start after you click 'ok'.");
                    resetField();
                    break;
                case " ok loser":
                    isGame = false;
                    isLoose = true;
                    stopWatch.Stop();
                    soundLoose.Play();
                    MessageBox.Show("New game will start after you click 'ok'.");
                    resetField();
                    break;
                case " extra":
                    isExtra = true;
                    addChat("You`ve destroyed all the ships but " + nameContestant + " had done less steps. " + nameContestant + " has a chance for standoff.");
                    break;
                case " standoff":
                    isGame = false;
                    stopWatch.Stop();
                    soundStandoff.Play();
                    addChat("You both are good players. Standoff this time.");
                    MessageBox.Show("New game will start after you click 'ok'.");
                    resetField();
                    break;
                case " time":
                    isGame = false;
                    addChat(nameContestant + " didn`t made a step for the allotted time. You`ve won!");
                    stopWatch.Reset();
                    send(" ok loser time");
                    //timer1.Stop();
                    MessageBox.Show("New game will start after you click 'ok'.");
                    resetField();
                    break;
                case " ok loser time":
                    isGame = false;
                    addChat("You didn`t made a step for the allotted time. You`ve lose.");
                    stopWatch.Stop();
                    MessageBox.Show("New game will start after you click 'ok'.");
                    resetField();
                    break;
                case " connection good":
                    isConnectionRealForTimer = false;
                    isConnectionReal = true;
                    break;

                default:
                    handleGameData(returnData);
                    break;
            }
            Invalidate();
        }

        #region ForDrawing
        private bool isLeftLine(int i, int j)
        {
            if (i - 1 >= 0)
                if (cellsMy[i - 1, j].isShip) return false;
                else return true;
            else
                return false;
        }
        
        private bool isRightLine(int i, int j)
        {
            if (i + 1 < fieldSize)
                if (cellsMy[i + 1, j].isShip) return false;
                else return true;
            else
                return false;
        }

        private bool isUpLine(int i, int j)
        {
            if (j - 1 >= 0)
                if (cellsMy[i, j - 1].isShip) return false;
                else return true;
            else
                return false;
        }
        
        private bool isDownLine(int i, int j)
        {
            if (j + 1 < fieldSize)
                if (cellsMy[i, j + 1].isShip) return false;
                else return true;
            else
                return false;
        }

        private int culculateAngle()
        {
            int angle = 0;

            if (isUp && !isDown && !isLeft && isRight)
            {
                angle = 45;
            }
            if (!isUp && !isDown && !isLeft && isRight)
            {
                angle = 90;
            }
            if (!isUp && isDown && !isLeft && isRight)
            {
                angle = 135;
            }
            if (!isUp && isDown && !isLeft && !isRight)
            {
                angle = 180;
            }
            if (!isUp && isDown && isLeft && !isRight)
            {
                angle = 225;
            }
            if (!isUp && !isDown && isLeft && !isRight)
            {
                angle = 270;
            }
            if (isUp && !isDown && isLeft && !isRight)
            {
                angle = 315;
            }

            return angle;
        }

        private void pushRockets(int i,int j, bool isContestantShoot)
        {
            correntI = i;
            correntJ = j;

            if (!isContestantShoot)
            {
                Point p = firstShipPoint();
                x1 = p.X;
                y1 = p.Y;
                x2 = cellsContestant[i, j].x + cellSize / 2;
                y2 = cellsContestant[i, j].y + cellSize / 2;
            }
            else
            {
                Point p = firstUnpushedPoint();
                x1 = p.X;
                y1 = p.Y;
                x2 = cellsMy[i, j].x + cellSize / 2;
                y2 = cellsMy[i, j].y + cellSize / 2;
            }
            isRocket = true;
        }

        private Point firstShipPoint()
        {
            Point p = new Point();

            for (int i = fieldSize - 1; i >= 0; i--)
            {
                bool isBreak = false;
                for (int j = 0; j < fieldSize; j++)
                {
                    if (cellsMy[i, j].isShip && !cellsMy[i, j].isPushed)
                    {
                        p.X = cellsMy[i, j].x;
                        p.Y = cellsMy[i, j].y;
                        isBreak = true;
                        break;
                    }
                }
                if (isBreak)
                    break;
            }

            return p;
        }

        private Point firstUnpushedPoint()
        {
            Point p = new Point();

            for (int i = 0; i < fieldSize; i++)
            {
                bool isBreak = false;
                for (int j = 0; j < fieldSize; j++)
                {
                    if (!cellsContestant[i, j].isPushed)
                    {
                        p.X = cellsContestant[i, j].x;
                        p.Y = cellsContestant[i, j].y;
                        isBreak = true;
                        break;
                    }
                }
                if (isBreak)
                    break;
            }

            return p;
        }
        #endregion

        #region ClicksOnMenuAndButtons
        private void connectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            disconnectToolStripMenuItem1.Enabled = true;
            connectToolStripMenuItem.Enabled = false;

            makeConnection = new MakeConnection(this);
            makeConnection.ShowDialog();
        }

        private void disconnectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            addChat("Connection had been lost.");
            send(" disconnect");
            closeConnections();
            isConnection = false;

            disconnectToolStripMenuItem1.Enabled = false;
        }

        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (isConnection)
            {
                send(" disconnect");
                closeConnections();
            }
            isClose = true;
            Close();
        }
        
        private void rulesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rules = new Rules(this);
            rules.ShowDialog();
        }

        private void textBoxForMessages_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                sendMessage();
            }
        }

        private void buttonSendMessage_Click(object sender, EventArgs e)
        {
            sendMessage();
        }

        private void savedDataChangeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            settings = new Settings(this);
            settings.ShowDialog();
        }
        #endregion

        #region FormEvents
        public MainForm()
        {
            InitializeComponent();
        }

        private void changeNameLabel(string str)
        {
            labelContestant.Invoke((MethodInvoker)(() => labelContestant.Text = str));
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!isRocket)
            {
                if (isGame)
                {
                    richTextBoxChat.Enabled = true;
                    textBoxForMessages.Enabled = true;
                    buttonSendMessage.Enabled = true;

                    if (isMyTurnPlay)
                    {
                        labelTimeContestant.Text = "";
                        TimeSpan ts = stopWatch.Elapsed;
                        string elapsedTime = String.Format("{0:00}:{1:00}",
                                            ts.Minutes, timeForStep - ts.Seconds);

                        labelTimeThis.Text = elapsedTime;
                        isTimeEnd(ts.Seconds);
                    }
                    else
                    {
                        labelTimeThis.Text = "";
                        TimeSpan ts = stopWatch.Elapsed;
                        string elapsedTime = String.Format("{0:00}:{1:00}",
                                            ts.Minutes, timeForStep - ts.Seconds);

                        labelTimeContestant.Text = elapsedTime;
                        isTimeEnd(ts.Seconds);
                    }
                }
                else
                {
                    richTextBoxChat.Enabled = false;
                    textBoxForMessages.Enabled = false;
                    buttonSendMessage.Enabled = false;
                }

                if ((isSendReady) && (!isAnswer))
                    send(" ready");
                if (isLoose)
                {
                    addChat("You`ve lost. Maybe next time :-) ");
                    isLoose = false;
                }

                if (!isConnectionReal)
                {
                    if (isConnectionRealForTimer)
                    {
                        closeConnections();
                        addChat("Connection with " + nameContestant + " had been lost.");
                        isConnectionReal = true;
                    }
                    isConnectionRealForTimer = true;
                }
                if (!isStep && isGame)
                {
                    for (int i = 0; i < fieldSize; i++)
                        for (int j = 0; j < fieldSize; j++)
                        {
                            cellsMy[i, j].isPushed = false;
                            cellsContestant[i, j].isPushed = false;
                        }
                    Invalidate();
                    isStep = true;
                }
            }
            else
            {
                timer1.Interval = 20;
                if ((x1 <= x2) && (Math.Abs(x1 - x2) > step))
                {
                    x1 += step;
                    isRight = true;
                }
                else
                {
                    isRight = false;
                }

                if ((x1 >= x2) && (Math.Abs(x1 - x2) > step))
                {
                    x1 -= step;
                    isLeft = true;
                }
                else
                {
                    isLeft = false;
                }

                if ((y1 <= y2) && (Math.Abs(y1 - y2) > step))
                {
                    y1 += step;
                    isDown = true;
                }
                else
                {
                    isDown = false;
                }

                if ((y1 >= y2) && (Math.Abs(y1 - y2) > step))
                {
                    y1 -= step;
                    isUp = true;
                }
                else
                {
                    isUp = false;
                }

                bool isBreak = false;
                if ((Math.Abs(x1 - x2) <= step) && (Math.Abs(y1 - y2) <= step))
                    isBreak = true;
                if (isBreak)
                {
                    isRocket = false;
                    timer1.Interval = 800;

                    if (isMyTurnPlay && isEnter)
                    {
                        pushOnCell(correntI, correntJ);
                        isEnter = false;
                    }
                }
                else
                {
                    Invalidate();
                }
            }
        }
        
        private void MainForm_Load(object sender, EventArgs e)
        {
            ControlBox = false;
            isMyTurnPlay = false;
            isAnswer = false;
            isStepPossible = true;

            for (int i = 0; i < fieldSize; i++)
            {
                for (int j = 0; j < fieldSize; j++)
                {
                    cellsMy[i, j] = new Cells(35 + i * cellSize, 105 + j * cellSize, cellSize);
                    cellsContestant[i, j] = new Cells(435 + i * cellSize, 105 + j * cellSize, cellSize);
                }
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if ((e.CloseReason == CloseReason.UserClosing) && !isClose)
            {
                e.Cancel = true;
            }
            else
            {
                Process.GetCurrentProcess().Kill();
            }
        }

        private void MainForm_MouseDown(object sender, MouseEventArgs e)
        {
            if (isMyTurnPlay && isGame && isStepPossible)
            {
                for (int i = 0; i < fieldSize; i++)
                {
                    bool isBreak = false;
                    for (int j = 0; j < fieldSize; j++)
                    {
                        if (cellsContestant[i, j].getRect().Contains(new Point(e.X, e.Y)))
                        {
                            if (!cellsContestant[i, j].isPushed)
                            {
                                isConnectionReal = false;
                                isStepPossible = false;
                                isStep = true;

                                stopWatch.Reset();
                                stopWatch.Start();
                                if (isPlusStepMy)
                                {
                                    isPlusStepMy = false;
                                    stepsMy++;
                                }

                                isEnter = true;
                                pushRockets(i, j, false);
                            }
                            isBreak = true;
                            break;
                        }
                    }
                    if (isBreak) break;
                }
            }
        }

        private void MainForm_Paint(object sender, PaintEventArgs e)
        {
            Graphics g1 = e.Graphics;

            Bitmap bm = new Bitmap(800, 445, g1);
            Graphics g = Graphics.FromImage(bm);

            g.FillRectangle(Brushes.White,
                cellsMy[0, 0].x - cellSize / 1.2f,
                cellsMy[0, 0].y - cellSize / 1.2f,
                (fieldSize + 1.44f) * cellSize,
                (fieldSize + 1.44f) * cellSize);
            g.FillRectangle(Brushes.White,
                cellsContestant[0, 0].x - cellSize / 1.2f,
                cellsContestant[0, 0].y - cellSize / 1.2f,
                (fieldSize + 1.44f) * cellSize,
                (fieldSize + 1.44f) * cellSize);

            Font font = new Font("Arial", 10);
            
            g.DrawString("Steps: "+stepsMy.ToString(), font,
                        Brushes.Black,
                        50, 30);
            g.DrawString("Steps: " + stepsContestant.ToString(), font,
                        Brushes.Black,
                        450, 30);

            font = new Font("Arial", 20);

            for (int i = 0; i < fieldSize; i++)
            {
                g.DrawLine(new Pen(Color.FromArgb(181, 226, 239)),
                    cellsMy[i, 0].x,
                    cellsMy[i, 0].y - cellSize / 2.0f,
                    cellsMy[i, 0].x,
                    cellsMy[i, 0].y + (cellSize + 1) * fieldSize * 1.0f);
                g.DrawLine(new Pen(Color.FromArgb(181, 226, 239)),
                    cellsMy[0, i].x - cellSize / 2.0f,
                    cellsMy[0, i].y,
                    cellsMy[0, i].x + (cellSize + 1) * fieldSize * 1.0f,
                    cellsMy[0, i].y);

                g.DrawLine(new Pen(Color.FromArgb(181, 226, 239)),
                    cellsContestant[i, 0].x,
                    cellsContestant[i, 0].y - cellSize / 2.0f,
                    cellsContestant[i, 0].x,
                    cellsContestant[i, 0].y + (cellSize + 1) * fieldSize * 1.0f);
                g.DrawLine(new Pen(Color.FromArgb(181, 226, 239)),
                    cellsContestant[0, i].x - cellSize / 2.0f,
                    cellsContestant[0, i].y,
                    cellsContestant[0, i].x + (cellSize + 1) * fieldSize * 1.0f,
                    cellsContestant[0, i].y);

                if (i == 8)
                {
                    g.DrawString(((char)(65 + i)).ToString(), font,
                        new SolidBrush(Color.FromArgb(86, 49, 196)),
                        cellsMy[0, i].x - cellSize * 0.7f,
                        cellsMy[0, i].y);
                    g.DrawString(((char)(65 + i)).ToString(), font,
                        new SolidBrush(Color.FromArgb(86, 49, 196)),
                        cellsContestant[0, i].x - cellSize * 0.7f,
                        cellsContestant[0, i].y);
                }
                else
                {
                    g.DrawString(((char)(65 + i)).ToString(), font,
                        new SolidBrush(Color.FromArgb(86, 49, 196)),
                        cellsMy[0, i].x - cellSize * 0.9f,
                        cellsMy[0, i].y);
                    g.DrawString(((char)(65 + i)).ToString(), font,
                            new SolidBrush(Color.FromArgb(86, 49, 196)),
                            cellsContestant[0, i].x - cellSize * 0.9f,
                            cellsContestant[0, i].y);
                }

                if (i == fieldSize - 1)
                {
                    g.DrawLine(new Pen(Color.FromArgb(181, 226, 239)),
                        cellsMy[i, 0].x + cellSize,
                        cellsMy[i, 0].y - cellSize / 2.0f,
                        cellsMy[i, 0].x + cellSize,
                        cellsMy[i, 0].y + (cellSize + 1) * fieldSize * 1.0f);
                    g.DrawLine(new Pen(Color.FromArgb(181, 226, 239)),
                        cellsMy[0, i].x - cellSize / 2.0f,
                        cellsMy[0, i].y + cellSize,
                        cellsMy[0, i].x + (cellSize + 1) * fieldSize * 1.0f,
                        cellsMy[0, i].y + cellSize);

                    g.DrawLine(new Pen(Color.FromArgb(181, 226, 239)),
                        cellsContestant[i, 0].x + cellSize,
                        cellsContestant[i, 0].y - cellSize / 2.0f,
                        cellsContestant[i, 0].x + cellSize,
                        cellsContestant[i, 0].y + (cellSize + 1) * fieldSize * 1.0f);
                    g.DrawLine(new Pen(Color.FromArgb(181, 226, 239)),
                        cellsContestant[0, i].x - cellSize / 2.0f,
                        cellsContestant[0, i].y + cellSize,
                        cellsContestant[0, i].x + (cellSize + 1) * fieldSize * 1.0f,
                        cellsContestant[0, i].y + cellSize);

                    g.DrawString((i + 1).ToString(), font,
                        new SolidBrush(Color.FromArgb(86, 49, 196)),
                        cellsMy[i, 0].x - cellSize / 5.0f,
                        cellsMy[i, 0].y - cellSize);
                    g.DrawString((i + 1).ToString(), font,
                        new SolidBrush(Color.FromArgb(86, 49, 196)),
                        cellsContestant[i, 0].x - cellSize / 5.0f,
                        cellsContestant[i, 0].y - cellSize);
                }
                else
                {
                    g.DrawString((i + 1).ToString(), font,
                        new SolidBrush(Color.FromArgb(86, 49, 196)),
                        cellsMy[i, 0].x + cellSize / 8.0f,
                        cellsMy[i, 0].y - cellSize);
                    g.DrawString((i + 1).ToString(), font,
                        new SolidBrush(Color.FromArgb(86, 49, 196)),
                        cellsContestant[i, 0].x + cellSize / 8.0f,
                        cellsContestant[i, 0].y - cellSize);
                }
            }

            for (int i = 0; i < fieldSize; i += fieldSize - 1)
            {
                //Внешние границы "ручкой"
                if ((i == 0))
                {
                    g.DrawLine(new Pen(Color.FromArgb(86, 49, 196), 2.0f),
                        cellsMy[i, 0].x,
                        cellsMy[i, 0].y,
                        cellsMy[i, 0].x,
                        cellsMy[i, 0].y + cellSize * fieldSize);
                    g.DrawLine(new Pen(Color.FromArgb(86, 49, 196), 2.0f),
                        cellsMy[0, i].x,
                        cellsMy[0, i].y,
                        cellsMy[0, i].x + cellSize * fieldSize,
                        cellsMy[0, i].y);

                    g.DrawLine(new Pen(Color.FromArgb(86, 49, 196), 2.0f),
                        cellsContestant[i, 0].x,
                        cellsContestant[i, 0].y,
                        cellsContestant[i, 0].x,
                        cellsContestant[i, 0].y + cellSize * fieldSize);
                    g.DrawLine(new Pen(Color.FromArgb(86, 49, 196), 2.0f),
                        cellsContestant[0, i].x,
                        cellsContestant[0, i].y,
                        cellsContestant[0, i].x + cellSize * fieldSize,
                        cellsContestant[0, i].y);
                }
                if (i == fieldSize - 1)
                {
                    g.DrawLine(new Pen(Color.FromArgb(86, 49, 196), 2.0f),
                        cellsMy[i, 0].x + cellSize,
                        cellsMy[i, 0].y,
                        cellsMy[i, 0].x + cellSize,
                        cellsMy[i, 0].y + cellSize * fieldSize);
                    g.DrawLine(new Pen(Color.FromArgb(86, 49, 196), 2.0f),
                        cellsMy[0, i].x,
                        cellsMy[0, i].y + cellSize,
                        cellsMy[0, i].x + cellSize * fieldSize,
                        cellsMy[0, i].y + cellSize);

                    g.DrawLine(new Pen(Color.FromArgb(86, 49, 196), 2.0f),
                        cellsContestant[i, 0].x + cellSize,
                        cellsContestant[i, 0].y,
                        cellsContestant[i, 0].x + cellSize,
                        cellsContestant[i, 0].y + cellSize * fieldSize);
                    g.DrawLine(new Pen(Color.FromArgb(86, 49, 196), 2.0f),
                        cellsContestant[0, i].x,
                        cellsContestant[0, i].y + cellSize,
                        cellsContestant[0, i].x + cellSize * fieldSize,
                        cellsContestant[0, i].y + cellSize);
                }
            }
            
            for (int i = 0; i < fieldSize; i++)
            {
                for (int j = 0; j < fieldSize; j++)
                {
                    //корабли (внешние границы)
                    if (cellsMy[i, j].isShip)
                    {
                        if (isLeftLine(i, j))
                            g.DrawLine(new Pen(Color.FromArgb(86, 49, 196), 2),
                                cellsMy[i, j].x, cellsMy[i, j].y,
                                cellsMy[i, j].x, cellsMy[i, j].y + cellSize);
                        if (isRightLine(i, j))
                            g.DrawLine(new Pen(Color.FromArgb(86, 49, 196), 2),
                                cellsMy[i, j].x + cellSize, cellsMy[i, j].y,
                                cellsMy[i, j].x + cellSize, cellsMy[i, j].y + cellSize);
                        if (isUpLine(i, j))
                            g.DrawLine(new Pen(Color.FromArgb(86, 49, 196), 2),
                                cellsMy[i, j].x, cellsMy[i, j].y,
                                cellsMy[i, j].x + cellSize, cellsMy[i, j].y);
                        if (isDownLine(i, j))
                            g.DrawLine(new Pen(Color.FromArgb(86, 49, 196), 2),
                                cellsMy[i, j].x, cellsMy[i, j].y + cellSize,
                                cellsMy[i, j].x + cellSize, cellsMy[i, j].y + cellSize);
                    }


                    //корабли (если подбиты или убиты) свои
                    if (cellsMy[i,j].isPushed && cellsMy[i, j].isShip)
                    {
                        g.DrawLine(new Pen(Color.FromArgb(86, 49, 196), 2),
                            cellsMy[i, j].x, cellsMy[i, j].y,
                            cellsMy[i, j].x + cellSize, cellsMy[i, j].y + cellSize);
                        g.DrawLine(new Pen(Color.FromArgb(86, 49, 196), 2),
                            cellsMy[i, j].x + cellSize, cellsMy[i, j].y,
                            cellsMy[i, j].x, cellsMy[i, j].y + cellSize);
                    }

                    if (cellsMy[i, j].isPushed && !cellsMy[i, j].isShip)
                    {
                        g.FillEllipse(new SolidBrush(Color.FromArgb(86, 49, 196)),
                            cellsMy[i, j].x + cellSize / 2 - 2,
                            cellsMy[i, j].y + cellSize / 2 - 2,
                            4, 4);
                    }

                    //корабли (если подбиты или убиты) соперника
                    if (cellsContestant[i, j].isPushed && cellsContestant[i, j].isShip)
                    {
                        g.DrawLine(new Pen(Color.FromArgb(86, 49, 196), 2),
                            cellsContestant[i, j].x, cellsContestant[i, j].y,
                            cellsContestant[i, j].x + cellSize, cellsContestant[i, j].y + cellSize);
                        g.DrawLine(new Pen(Color.FromArgb(86, 49, 196), 2),
                            cellsContestant[i, j].x + cellSize, cellsContestant[i, j].y,
                            cellsContestant[i, j].x, cellsContestant[i, j].y + cellSize);
                    }

                    if (cellsContestant[i, j].isPushed && !cellsContestant[i, j].isShip)
                    {
                        g.FillEllipse(new SolidBrush(Color.FromArgb(86, 49, 196)),
                            cellsContestant[i, j].x + cellSize / 2 - 2,
                            cellsContestant[i, j].y + cellSize / 2 - 2,
                            4, 4);
                    }
                }
            }

            g1.DrawImage(bm, 0, 0);
            if (isRocket)
            {
                int a = culculateAngle();

                g1.TranslateTransform(x1, y1);
                g1.RotateTransform(a);

                g1.DrawImage(rocketBitmap, 0, 0);
                
                g1.TranslateTransform(-x1, -y1);
                g1.RotateTransform(-a);
            }
        }
        #endregion
    }
}
